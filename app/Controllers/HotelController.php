<?php

namespace Trivago\Controllers;


use Trivago\Services\DataIntegrationService;

class HotelController
{
    function __construct()
    {
    }

    /**
     * @return HotelController
     */
    public static function create() : HotelController
    {
        return new HotelController();
    }

    /**
     * Return json of hotel rooms sorted by price in ascending order
     *
     * @return string
     */
    public function getRooms() : string
    {
        $obj = new DataIntegrationService();
        echo json_encode($obj->getDataFromSources());
    }
}