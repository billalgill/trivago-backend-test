<?php


namespace Trivago\Advertisers;

use Trivago\Interfaces\IAdvertiser;
use Trivago\Interfaces\IMapper;
use Trivago\Utils\Constants;

class AdvertiserService implements IAdvertiser
{
    private IMapper $mDataMapper;
    private string $mURL;

    /**
     * AdvertiserService constructor.
     *
     * Advertiser which is responsible for the data
     * Data Mapper is injected in the constructor, so that if data from advertiser is changed
     * like from json to xml so we only need to pass IMapper of XMLMapper
     *
     * @param IMapper $dataMapper
     * @param string $URL
     */
    public function __construct(IMapper $dataMapper, string $URL)
    {
        $this->mDataMapper = $dataMapper;
        $this->mURL = $URL;
    }

    /**
     * Get data from the specifeid Url
     * Map data to the keys specified in the Metadata
     *
     * @return array
     */
    public function getData() : array
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->mURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);

        $responseData = $this->mDataMapper->mapData($response);
        curl_close($ch);

        return $responseData;
    }
}