<?php

namespace Trivago\Mapper;

use Trivago\Config\Constants;
use Trivago\Interfaces\IMapper;
use Trivago\Utils\MetaData;

class JSONMapper implements IMapper
{
    /**
     * Validate if JSON is valid
     * Map JSON data to the consistent key e.g. "net_price" & "net_rate" both are mapped to "netPrice"
     * "total" & "totalPrice" both are mapped to "totalPrice"
     *
     * @param string $data
     * @return mixed
     */
    public function mapData(string $data)
    {
        $response[Constants::RESPONSE_CODE] = Constants::SUCCESS;
        $jsonData = json_decode($data, true);
        $hotelsRooms = array();

        if(is_null($jsonData))
        {
            $response[Constants::RESPONSE_CODE] = Constants::ERROR;
            $response[Constants::RESPONSE_MESSAGE] = "Invalid JSON";

            return $response;
        }
        else
        {
            if(!isset($jsonData["hotels"]))
            {
                $response[Constants::RESPONSE_CODE] = Constants::ERROR;
                $response[Constants::RESPONSE_MESSAGE] = "Hotels not Found";

                return $response;
            }
            else
            {
                foreach ($jsonData["hotels"] as $key=>$value)
                {
                    if(isset($value["rooms"]))
                    {
                        foreach ($value["rooms"] as $roomsData)
                        {
                            $roomInfo = array();
                            $roomInfo["hotelName"] = $value["name"];
                            $roomInfo["stars"] = $value["stars"];

                            foreach ($roomsData as $roomKey=>$roomValue)
                            {
                                if(isset(MetaData::$mappingRoomInfo[$roomKey]))
                                {
                                    if(MetaData::$mappingRoomInfo[$roomKey] == "taxes")
                                    {
                                        if(array_keys($roomValue) !== range(0, count($roomValue) - 1))
                                        {
                                            $roomInfo[MetaData::$mappingRoomInfo[$roomKey]][] = $roomValue;
                                        }
                                        else
                                        {
                                            $roomInfo[MetaData::$mappingRoomInfo[$roomKey]] = $roomValue;
                                        }
                                    }
                                    else
                                    {
                                        $roomInfo[MetaData::$mappingRoomInfo[$roomKey]] = $roomValue;
                                    }
                                }
                            }

                            $hotelsRooms[] = $roomInfo;
                        }
                    }
                }
            }
        }

        $response[Constants::RESPONSE_DATA] = $hotelsRooms;

        return $response;
    }
}