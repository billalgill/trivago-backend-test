<?php

namespace Trivago\Interfaces;

interface IMapper{
    public function mapData(string $input);
}
