<?php


namespace Trivago\Config;


class Constants
{
    public const URL_ADVERTISER1 = "https://f704cb9e-bf27-440c-a927-4c8e57e3bad1.mock.pstmn.io/s1/availability";
    public const URL_ADVERTISER2 = "https://f704cb9e-bf27-440c-a927-4c8e57e3bad1.mock.pstmn.io/s2/availability";
    public const RESPONSE_CODE = "code";
    public const SUCCESS = 0;
    public const ERROR = 1;
    public const RESPONSE_MESSAGE = "message";
    public const RESPONSE_DATA = "data";
}