<?php


namespace Trivago\Utils;


class MetaData
{
    public static array $mappingRoomInfo;

    /**
     * Mapping of related keys from different sources to the single consisitent keys
     */
    public static function setMappingData()
    {
        MetaData::$mappingRoomInfo["code"] = "code";
        MetaData::$mappingRoomInfo["name"] = "name";
        MetaData::$mappingRoomInfo["net_rate"] = "netPrice";
        MetaData::$mappingRoomInfo["net_price"] = "netPrice";
        MetaData::$mappingRoomInfo["total"] = "totalPrice";
        MetaData::$mappingRoomInfo["totalPrice"] = "totalPrice";
        MetaData::$mappingRoomInfo["taxes"] = "taxes";
    }

}