<?php


namespace Trivago\Services;


use Trivago\Advertisers\AdvertiserService;
use Trivago\Mapper\JSONMapper;
use Trivago\Config\Constants;

class DataIntegrationService
{
    private array $dataSources;
    private array $rooms;

    /***
     * Advertisers are initialized from which we could fetch data
     * If we want to add new advertiser we will add here
     *
     * DataIntegrationService constructor.
     */
    public function __construct()
    {
        $this->dataSources[] = new AdvertiserService(new JSONMapper(), Constants::URL_ADVERTISER1);
        $this->dataSources[] = new AdvertiserService(new JSONMapper(), Constants::URL_ADVERTISER2);
        $this->rooms = array();
    }

    /**
     * Get data from advertisers
     * Merge data from different sources and then send to the sort function
     *
     * return sorted list of rooms
     * @return array
     */
    public function getDataFromSources() : array
    {
        $response[Constants::RESPONSE_CODE] = Constants::SUCCESS;

        if(count($this->dataSources) > 0)
        {
            foreach ($this->dataSources as $source)
            {
                $responseData = $source->getData();
                if($responseData[Constants::RESPONSE_CODE] == Constants::SUCCESS && isset($responseData[Constants::RESPONSE_DATA]))
                {
                    $this->rooms = array_merge($this->rooms, $responseData[Constants::RESPONSE_DATA]);
                }
                else
                {
                    $response[Constants::RESPONSE_CODE] = $responseData[Constants::RESPONSE_CODE];
                    $response[Constants::RESPONSE_MESSAGE] = $responseData[Constants::RESPONSE_MESSAGE];

                    return $response;
                }
            }

            $this->sortData();

            if(empty($this->rooms))
            {
                $response[Constants::RESPONSE_CODE] = Constants::ERROR;
                $response[Constants::RESPONSE_MESSAGE] = "Room Data not Found !!!";

                return $response;
            }

            $response[Constants::RESPONSE_DATA] = $this->rooms;
            return $response;
        }
        else{
            $response[Constants::RESPONSE_CODE] = Constants::ERROR;
            $response[Constants::RESPONSE_MESSAGE] = "Data Sources not Found !!!";

            return $response;
        }
    }

    /**
     * Sort rooms data based on total price in ascending order
     */
    private function sortData()
    {
        $price = array_column($this->rooms, 'total');
        $totalPrice = array_column($this->rooms, 'totalPrice');
        $price = array_merge($price, $totalPrice);
        array_multisort($price, SORT_DESC, $this->rooms);

        $this->rooms = array_column($this->rooms, null, 'code');
        $this->rooms = array_reverse($this->rooms);
    }
}