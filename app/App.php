<?php

namespace Trivago;

use Trivago\Config\Constants;
use Trivago\Utils\MetaData;

class App
{
    private $controller = null;
    private ?string $action = null;

    function __construct()
    {
        $response[Constants::RESPONSE_CODE] = Constants::SUCCESS;

        /*Get Query Parameters and pass to respective controller*/
        if (isset($_SERVER['REQUEST_URI'])) {
            $parts = parse_url($_SERVER['REQUEST_URI']);

            if (isset($_GET['controller'])) {
                $this->controller = $_GET["controller"]."Controller";
            }
            else
            {
                $response[Constants::RESPONSE_CODE] = Constants::ERROR;
                $response[Constants::RESPONSE_MESSAGE] = "Controller not provided";
                return json_encode($response);
            }

            if (isset($_GET['action'])) {
                $this->action = $_GET["action"];
            }
            else
            {
                $response[Constants::RESPONSE_CODE] = Constants::ERROR;
                $response[Constants::RESPONSE_MESSAGE] = "Action not provided";
                return json_encode($response);
            }
        }

        MetaData::setMappingData();
        $this->run();
    }

    /**
     * Main function of Application to start running application
     */
    public function run()
    {
        if (file_exists(APP . 'Controllers/' . $this->controller . '.php')) {
//            require APP . 'controller/' . $this->controller . '.php';
            $controller = "Trivago\\Controllers\\" . ucfirst($this->controller);
            $this->controller = $controller::create();
            $controllerAction = $this->action;
            $this->controller->$controllerAction();

        } else {

            $response[Constants::RESPONSE_CODE] = Constants::ERROR;
            $response[Constants::RESPONSE_MESSAGE] = "Controller not found";
            return json_encode($response);
        }
    }

}