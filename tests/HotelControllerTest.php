<?php

namespace Trivago;

use Trivago\Config\Constants;
use Trivago\Controllers\HotelController;
use PHPUnit\Framework\TestCase;

class HotelControllerTest extends TestCase
{
    private HotelController $hotelController;

    protected function setUp() : void
    {
        $this->hotelController = new HotelController();
    }

    public function testCreate()
    {
        $this->assertEquals($this->hotelController, HotelController::create());
    }
}
