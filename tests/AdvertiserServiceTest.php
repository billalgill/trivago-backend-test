<?php

namespace Trivago;

use PHPUnit\Framework\TestCase;
use Trivago\Advertisers\AdvertiserService;
use Trivago\Config\Constants;
use Trivago\Mapper\JSONMapper;

class AdvertiserServiceTest extends TestCase
{
    public function testInvalidURL()
    {
        $advertiser = new AdvertiserService(new JSONMapper(), "www.blabla.com");
        $data = $advertiser->getData();
        $responseCode = $data[Constants::RESPONSE_CODE];
        $this->assertEquals(Constants::ERROR,$responseCode);
    }

    public function testNotEmpty()
    {
        $advertiser = new AdvertiserService(new JSONMapper(), Constants::URL_ADVERTISER1);
        $this->assertNotEmpty($advertiser->getData());
    }

    public function testAdvertiserData()
    {
        $advertiser = new AdvertiserService(new JSONMapper(), Constants::URL_ADVERTISER1);
        $data = $advertiser->getData();
        $responseCode = $data[Constants::RESPONSE_CODE];
        $this->assertEquals(Constants::SUCCESS,$responseCode);
    }
}
