# Trivage Hotel Rooms
A php web application that return hotel rooms sorted by price in ascending order.  

## Features

A REST API that gives hotel rooms sorted by price in ascending order.  

## Requirements

- PHP 7.4.*
- basic knowledge of OOP and Composer
- phpunit 6.5.*

## Installation
Need Apache2 and PHP 7.4.* to run on the localhost

## Assumptions

- You already have a setup of Apache2 and PHP 7.4.* on system

## Usage Examples (on localhost)

- [GET] http://localhost/trivago-backend-test/app/?controller=Hotel&action=getRooms

## Docker Commands
Below are the command to run into docker container

- docker build -t trivago .
- docker run -it --rm -p 8082:80 trivago:latest
- curl http://localhost:8082/trivago-backend-test/app/?controller=Hotel&action=getRooms


## Units Tests
- Unit Tests are included into the tests directory.

## Notes

- The code is extendable with any kind of data type. If we want to add a provider which 
gives XML data we just need to create new XMLMapper which implements IMapper and pass to advertiser without changing 
any other class our job would be done.
- If we have different advertiser which reads data from file system we just need to create 
new advertiserService which implements IAdvertiser and in getData() we could do our work without changing 
any other classes
 

    